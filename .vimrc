set ls=2
source $VIMRUNTIME/macros/matchit.vim
let b:match_words = '<%:%>,{:},(:),[:]'
map <F1> gT
map <F2> gt
map <silent><F3> :set wrap!<CR>
map <F4> :set ic!<CR>
map <silent><F5> :set number!<CR>
set wildmenu
set wildmode=list:longest,full
set number
